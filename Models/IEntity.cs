namespace BlogProvider.Models
{
    public interface IEntity
    {
        string Id { get; set; }
    }
}