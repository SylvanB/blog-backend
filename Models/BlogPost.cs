using System;
using BlogProvider.Services;
using MongoDB.Bson.Serialization.Attributes;

namespace BlogProvider.Models
{
    public class BlogPost : IEntity
    {
        [BsonId]
        public string Id { get; set; }

        public DateTime Date { get; set; }

        public string Author { get; set; }

        public string Title { get; set; }

        public string Description { get; set; }
        
        public string Content { get; set; }


    }
}
