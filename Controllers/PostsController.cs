﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BlogProvider.Models;
using BlogProvider.Services;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace BlogProvider.Controllers
{

    [Route("api/[controller]")]
    [ApiController]
    [EnableCors("MyPolicy")]
    public class PostsController : ControllerBase
    {
        private readonly ILogger<PostsController> logger;
        private readonly IRepository<BlogPost> repo;

        public PostsController(ILogger<PostsController> logger, IRepository<BlogPost> repo)
        {
            this.logger = logger;
            this.repo = repo;
        }

        [HttpGet("{id}")]
        public async Task<BlogPost> Get(string id)
        {
            return await this.repo.GetById(id);
        }

        [HttpGet("All")]
        public async Task<IEnumerable<BlogPost>> GetAll()
        {
            return await this.repo.GetAll();
        }


        // [HttpGet("GenTest")]
        // public async Task<BlogPost> GetGenTest()
        // {
        //     var post = new BlogPost() {
        //         Id = Guid.NewGuid().ToString(),
        //         Author = "Sylvan B",
        //         Date = DateTime.UtcNow,
        //         Title = "Test Post",
        //         Description = "Test Description",
        //         Content = "Hello World!"
        //     };

        //     await repo.Add(post);

        //     return post;
        // }
    }
}
