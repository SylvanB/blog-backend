using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using BlogProvider.Models;

namespace BlogProvider.Services
{

    public interface IRepository<T> where T : IEntity
    {
        Task<T> GetById(string id);
        Task<IEnumerable<T>> GetAll();
        
        Task<T> GetWhere(Expression<Func<T, bool>> predicate);
        
        Task Add(T entity);
        
        void Delete(T entity);
        
        void Update(T entity);

    }

}