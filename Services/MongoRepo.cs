using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using BlogProvider.Models;
using MongoDB.Bson;
using MongoDB.Driver;

namespace BlogProvider.Services {

    public class MongoRepo<T> : IRepository<T> where T : IEntity
    {

        private readonly IMongoCollection<T> collection;

        public MongoRepo() 
        {
            this.collection = new MongoClient("mongodb://localhost:27017")
                            .GetDatabase("blog")
                            .GetCollection<T>(typeof(T).ToString());
        }

        public async Task Add(T entity)
        {
            await this.collection.InsertOneAsync(entity);
        }

        public void Delete(T entity)
        {
            throw new NotImplementedException();
        }

        public async Task<T> GetById(string id)
        {
            return await collection.Find(x => x.Id == id).FirstOrDefaultAsync();
        }

        public async Task<IEnumerable<T>> GetAll()
        {
            
            return await collection.Find(_ => true).ToListAsync();
        }

        public async Task<T> GetWhere(Expression<Func<T, bool>> predicate)
        {
            return await collection.Find(predicate).FirstOrDefaultAsync();
        }

        public void Update(T entity)
        {
            throw new NotImplementedException();
        }
    }

}